package com.example.firstspringbootapplication.api;

import com.example.firstspringbootapplication.Person;
import com.example.firstspringbootapplication.PersonService;
import com.example.firstspringbootapplication.dao.Person;
import com.example.firstspringbootapplication.dao.PersonService;

public class PersonController {
    private final PersonService personService;


    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    public void addPerson(Person person){
        personService.addPerson(person);

    }
}
