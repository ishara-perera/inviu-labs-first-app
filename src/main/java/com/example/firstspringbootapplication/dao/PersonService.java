package com.example.firstspringbootapplication.dao;

public class PersonService {
    public final PersonDao personDao;

    public PersonService(PersonDao personDao){
        this.personDao = personDao;
    }

    public int addPerson(Person person){
        return personDao.insertPerson(person);
    }
}
