package com.example.firstspringbootapplication.dao;

import java.util.UUID;

public interface PersonDao {
    int insertPerson(Person person);

    default int addPerson(Person person){
        UUID id = UUID.randomUUID();
        return insertPerson(person);
    }


    int insertPerson(UUID id, Person person);
}
